#include <data_sglton.h>



// This is not the declaration of a Singleton object; we are setting 
// the private pointer variable "static Singleton *s_Instance" to 0.
// In java, we would simply set the pointer to 0 at declaration, but
// c++ don't allow that.
// We also couldn't put it in the constructor since "get" function is
// called before the constructor, and "get" need to see if it's 0 or not
Data_sglton *Data_sglton::s_Instance = 0;


// Constructor is only called once when *Singleton::get() is called the first time.
Data_sglton::Data_sglton()
{
}


Data_sglton *Data_sglton::get()
{
    if(!s_Instance)
        s_Instance = new Data_sglton;
    return s_Instance;
}    
