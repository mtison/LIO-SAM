#include "utility.h"
#include "data_sglton.h"
#include "lio_sam/cloud_info.h"
#include "lio_sam/save_map.h"

#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>

#include <nodelet/nodelet.h>
#include <std_msgs/String.h>
#include <pluginlib/class_list_macros.h>


using gtsam::symbol_shorthand::X; // Pose3 (x,y,z,r,p,y)
using gtsam::symbol_shorthand::V; // Vel   (xdot,ydot,zdot)
using gtsam::symbol_shorthand::B; // Bias  (ax,ay,az,gx,gy,gz)
using gtsam::symbol_shorthand::G; // GPS pose




namespace nodelet_ns
{

    class mapOptimization : public nodelet::Nodelet
    {

    public:




        //// Publisher/subscriber handles ////
        ros::Publisher pubLaserCloudSurround;
        ros::Publisher pubLaserOdometryGlobal;
        ros::Publisher pubLaserOdometryIncremental;
        ros::Publisher pubKeyPoses;
        ros::Publisher pubPath;
        ros::Publisher pubHistoryKeyFrames;
        ros::Publisher pubIcpKeyFrames;
        ros::Publisher pubRecentKeyFrames;
        ros::Publisher pubRecentKeyFrame;
        ros::Publisher pubCloudRegisteredRaw;
        ros::Publisher pubLoopConstraintEdge;
        ros::Subscriber subCloud;
        ros::Subscriber subGPS;
        ros::Subscriber subLoop;
        //////////////////////////////////////

        /////////// Data singleton ///////////
        Data_sglton* data;        
        //////////////////////////////////////



        lio_sam::cloud_info cloudInfo;

        

        pcl::PointCloud<PointType>::Ptr laserCloudCornerLast; // corner feature set from odoOptimization
        pcl::PointCloud<PointType>::Ptr laserCloudSurfLast; // surf feature set from odoOptimization
        pcl::PointCloud<PointType>::Ptr laserCloudCornerLastDS; // downsampled corner featuer set from odoOptimization
        pcl::PointCloud<PointType>::Ptr laserCloudSurfLastDS; // downsampled surf featuer set from odoOptimization

        pcl::PointCloud<PointType>::Ptr laserCloudOri;
        pcl::PointCloud<PointType>::Ptr coeffSel;

        std::vector<PointType> laserCloudOriCornerVec; // corner point holder for parallel computation
        std::vector<PointType> coeffSelCornerVec;
        std::vector<bool> laserCloudOriCornerFlag;
        std::vector<PointType> laserCloudOriSurfVec; // surf point holder for parallel computation
        std::vector<PointType> coeffSelSurfVec;
        std::vector<bool> laserCloudOriSurfFlag;

        map<int, pair<pcl::PointCloud<PointType>, pcl::PointCloud<PointType>>> laserCloudMapContainer;
        pcl::PointCloud<PointType>::Ptr laserCloudCornerFromMap;
        pcl::PointCloud<PointType>::Ptr laserCloudSurfFromMap;
        pcl::PointCloud<PointType>::Ptr laserCloudCornerFromMapDS;
        pcl::PointCloud<PointType>::Ptr laserCloudSurfFromMapDS;

        pcl::KdTreeFLANN<PointType>::Ptr kdtreeCornerFromMap;
        pcl::KdTreeFLANN<PointType>::Ptr kdtreeSurfFromMap;

        pcl::KdTreeFLANN<PointType>::Ptr kdtreeSurroundingKeyPoses;
        pcl::KdTreeFLANN<PointType>::Ptr kdtreeHistoryKeyPoses;

        pcl::VoxelGrid<PointType> downSizeFilterCorner;
        pcl::VoxelGrid<PointType> downSizeFilterSurf;
        pcl::VoxelGrid<PointType> downSizeFilterICP;
        pcl::VoxelGrid<PointType> downSizeFilterSurroundingKeyPoses; // for surrounding key poses of scan-to-map optimization
        
        ros::Time timeLaserInfoStamp;
        double timeLaserInfoCur;

        // transformTobeMapped is T_odom^new_node given by optimization or scan matching
        float transformTobeMapped[6];

        std::mutex mtx;
        std::mutex mtxLoopInfo;

        bool isDegenerate = false;
        cv::Mat matP;

        int laserCloudCornerFromMapDSNum = 0;
        int laserCloudSurfFromMapDSNum = 0;
        int laserCloudCornerLastDSNum = 0;
        int laserCloudSurfLastDSNum = 0;

        map<int, int> loopIndexContainer; // from new to old
        vector<pair<int, int>> loopIndexQueue;
        vector<gtsam::Pose3> loopPoseQueue;
        vector<gtsam::noiseModel::Diagonal::shared_ptr> loopNoiseQueue;
        deque<std_msgs::Float64MultiArray> loopInfoVec;


        Eigen::Affine3f transPointAssociateToMap;
        Eigen::Affine3f incrementalOdometryAffineFront;
        Eigen::Affine3f incrementalOdometryAffineBack;


        mapOptimization(void)
        {
        }


        virtual void onInit()
        {
            //ros::NodeHandle nh;
            ros::NodeHandle& nh = getNodeHandle();


            data = Data_sglton::get();

            // ISAM2 initialization
            gtsam::ISAM2Params parameters;
            parameters.relinearizeThreshold = 0.1;
            parameters.relinearizeSkip = 1;
            data->isam = new gtsam::ISAM2(parameters);
            data->optimize_flag = false;

            // All publishing topics
            pubKeyPoses                 = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/trajectory", 1);
            pubLaserCloudSurround       = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/map_global", 1);
            pubLaserOdometryGlobal      = nh.advertise<nav_msgs::Odometry> ("lio_sam/mapping/odometry", 1);
            pubLaserOdometryIncremental = nh.advertise<nav_msgs::Odometry> ("lio_sam/mapping/odometry_incremental", 1);
            pubPath                     = nh.advertise<nav_msgs::Path>("lio_sam/mapping/path", 1);
            pubHistoryKeyFrames   = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/icp_loop_closure_history_cloud", 1);
            pubIcpKeyFrames       = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/icp_loop_closure_corrected_cloud", 1);
            pubLoopConstraintEdge = nh.advertise<visualization_msgs::MarkerArray>("/lio_sam/mapping/loop_closure_constraints", 1);
            pubRecentKeyFrames    = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/map_local", 1); // We see here that subkeyframes corresponds to the local map
            pubRecentKeyFrame     = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/cloud_registered", 1);
            pubCloudRegisteredRaw = nh.advertise<sensor_msgs::PointCloud2>("lio_sam/mapping/cloud_registered_raw", 1);

            // All subscribing topics
            subCloud = nh.subscribe<lio_sam::cloud_info>("lio_sam/feature/cloud_info", 1, &mapOptimization::laserCloudInfoHandler, this, ros::TransportHints().tcpNoDelay());
            subGPS   = nh.subscribe<nav_msgs::Odometry> ("odometry/gpsz", 200, &mapOptimization::gpsHandler, this, ros::TransportHints().tcpNoDelay());
            //subLoop  = nh.subscribe<std_msgs::Float64MultiArray>("lio_loop/loop_closure_detection", 1, &mapOptimization::loopInfoHandler, this, ros::TransportHints().tcpNoDelay());

            // All services
            //srvSaveMap  = nh.advertiseService("lio_sam/save_map", &mapOptimization::saveMapService, this);


            // Initialization of the voxel grid  pcl::VoxelGrid<PointType>, which are point clouds have have been filtered by a voxel filter 
            downSizeFilterCorner.setLeafSize(0.2, 0.2, 0.2); //mappingCornerLeafSize=0.2
            downSizeFilterSurf.setLeafSize(0.4, 0.4, 0.4); //mappingSurfLeafSize=0.4
            downSizeFilterICP.setLeafSize(0.4, 0.4, 0.4); //mappingSurfLeafSize=0.4
            downSizeFilterSurroundingKeyPoses.setLeafSize(2.0, 2.0, 2.0); // surroundingKeyframeDensity=2.0; for surrounding key poses of scan-to-map optimization

            // Allocating memory
            data->cloudKeyPoses3D.reset(new pcl::PointCloud<PointType>());

            kdtreeSurroundingKeyPoses.reset(new pcl::KdTreeFLANN<PointType>());
            kdtreeHistoryKeyPoses.reset(new pcl::KdTreeFLANN<PointType>());

            laserCloudCornerLast.reset(new pcl::PointCloud<PointType>()); // corner feature set from odoOptimization
            laserCloudSurfLast.reset(new pcl::PointCloud<PointType>()); // surf feature set from odoOptimization
            laserCloudCornerLastDS.reset(new pcl::PointCloud<PointType>()); // downsampled corner featuer set from odoOptimization
            laserCloudSurfLastDS.reset(new pcl::PointCloud<PointType>()); // downsampled surf featuer set from odoOptimization

            laserCloudOri.reset(new pcl::PointCloud<PointType>());
            coeffSel.reset(new pcl::PointCloud<PointType>());

            laserCloudOriCornerVec.resize(16 * 1800); //N_SCAN * Horizon_SCAN
            coeffSelCornerVec.resize(16 * 1800);
            laserCloudOriCornerFlag.resize(16 * 1800);
            laserCloudOriSurfVec.resize(16 * 1800);
            coeffSelSurfVec.resize(16 * 1800);
            laserCloudOriSurfFlag.resize(16 * 1800);

            std::fill(laserCloudOriCornerFlag.begin(), laserCloudOriCornerFlag.end(), false);
            std::fill(laserCloudOriSurfFlag.begin(), laserCloudOriSurfFlag.end(), false);

            laserCloudCornerFromMap.reset(new pcl::PointCloud<PointType>());
            laserCloudSurfFromMap.reset(new pcl::PointCloud<PointType>());
            laserCloudCornerFromMapDS.reset(new pcl::PointCloud<PointType>());
            laserCloudSurfFromMapDS.reset(new pcl::PointCloud<PointType>());

            kdtreeCornerFromMap.reset(new pcl::KdTreeFLANN<PointType>());
            kdtreeSurfFromMap.reset(new pcl::KdTreeFLANN<PointType>());

            for (int i = 0; i < 6; ++i){
                transformTobeMapped[i] = 0;
            }

            matP = cv::Mat(6, 6, CV_32F, cv::Scalar::all(0));

        }




        // TOPIC lio_sam/feature/cloud_info callback
        // We enter this function at around 10hz (lidar rate)
        void laserCloudInfoHandler(const lio_sam::cloud_infoConstPtr& msgIn)
        {
            // extract time stamp
            timeLaserInfoStamp = msgIn->header.stamp;
            timeLaserInfoCur = msgIn->header.stamp.toSec();

            // extract info and feature cloud
            cloudInfo = *msgIn;
            pcl::fromROSMsg(msgIn->cloud_corner,  *laserCloudCornerLast);
            pcl::fromROSMsg(msgIn->cloud_surface, *laserCloudSurfLast);

            std::lock_guard<std::mutex> lock(mtx);

            static double timeLastProcessing = -1;
            if (timeLaserInfoCur - timeLastProcessing >= 0.15) //mappingProcessInterval=0.15
            {
                timeLastProcessing = timeLaserInfoCur;

                std::cout << "----------ENTER---------" << std::endl;
                ////////////////////////////////////////// SCAN MATCHING /////////////////////////////////////////////////
                ////////////////////////////////// #1 ///////////////////////////////////
                /* Here we obtain the initial estimation for the scanmatching algorithm
                 * This estimate is T_odom^new_node since scanmatching is done in odom
                 * If the estimate from imuPreintegration is available, use that
                 * Else, use estimate from imu AHRS and previous position xyz 
                 * INPUTS : -Imupreintegration prediction from feature/cloud_info
                 * 		-imu ahrs from feature/cloud_info
                 * 		-transformTobeMapped of the previous graph optimization which corresponds to the previous T_odom^node
                 * OUTPUTS: -transformTobeMapped updated by imupreintegration if available, or else by imu ahrs
                 * 			(this transformTobeMapped is the initial estimate for the scan matching algorithm)
                 */
                //////// HERE IS HOW INFORMATION FROM IMUPREINTEGRATION IS FUSED TO THE MAP OPTIMIZATION.
                //////// IT ONLY USES THE INFORMATION AS AN INITIAL ESTIMATION FOR THE SCANMATCHING PROCESS
                updateInitialGuess();

                ////////////////////////////////// #2 ///////////////////////////////////
                extractSurroundingKeyFrames(); 

                ////////////////////////////////// #3 ///////////////////////////////////
                downsampleCurrentScan();

                ////////////////////////////////// #4 ///////////////////////////////////
                /* Scan-matching
                 * INPUTS:
                 * OUTPUTS:
                 * 		- Transform and covariance from scanmatching (not accepted yet)
                 */
                scan2MapOptimization();
                //////////////////////////////////////////////////////////////////////////////////////////////////////////


                ///////////////////////////////////////// MAP OPTIMIZATION ///////////////////////////////////////////////
                ////////////////////////////////// #5 ///////////////////////////////////
                /* Nodes and Constraints are added here.
                 * We enter saveKeyFramesAndFactor at around 10hz, but there is a condition inside that
                 * rejects new scans if the movement is not big enough
                 * INPUTS:
                 * 		- Previous graph (pose path and current pose)
                 * 		- Transform and covariance from scanmatching (not accepted yet)
                 * 		- Transform and covariance from GPS
                 */
                saveKeyFramesAndFactor();


                ////////////////////////////////// #6 ///////////////////////////////////
                /* iSAM2 optimization is done here.
                 * OUTPUTS:
                 * 		- Optimized graph (pose path and current pose)
                 */
                optimizeGraph();
    
                //////////////////////////////////////////////////////////////////////////////////////////////////////////

                ////////////////////////////////// #7 ///////////////////////////////////
                publishData();
            }
        }

        // TOPIC /odometry/gps callback
        void gpsHandler(const nav_msgs::Odometry::ConstPtr& gpsMsg)
        {
            // Queue front: oldest gps,   Queue back: newest gps
            // Each time a node is added, we pop the front up to a gps that is close to the keyframe time
            // see addGPSFactor
            data->gps_mutex.lock();
            data->gpsQueue.push_back(*gpsMsg);
            data->gps_mutex.unlock();
        }









        



        pcl::PointCloud<PointType>::Ptr transformPointCloud2(pcl::PointCloud<PointType>::Ptr cloudIn, pose6D* transformIn)
        {
            pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());

            int cloudSize = cloudIn->size();
            cloudOut->resize(cloudSize);

            Eigen::Affine3f transCur = pcl::getTransformation(transformIn->x, transformIn->y, transformIn->z, transformIn->roll, transformIn->pitch, transformIn->yaw);
            
            // numberOfCores=4
            #pragma omp parallel for num_threads(4)
            for (int i = 0; i < cloudSize; ++i)
            {
                const auto &pointFrom = cloudIn->points[i];
                cloudOut->points[i].x = transCur(0,0) * pointFrom.x + transCur(0,1) * pointFrom.y + transCur(0,2) * pointFrom.z + transCur(0,3);
                cloudOut->points[i].y = transCur(1,0) * pointFrom.x + transCur(1,1) * pointFrom.y + transCur(1,2) * pointFrom.z + transCur(1,3);
                cloudOut->points[i].z = transCur(2,0) * pointFrom.x + transCur(2,1) * pointFrom.y + transCur(2,2) * pointFrom.z + transCur(2,3);
                cloudOut->points[i].intensity = pointFrom.intensity;
            }
            return cloudOut;
        }

        float pointDistance(PointType p1, PointType p2)
        {
            return sqrt((p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y) + (p1.z-p2.z)*(p1.z-p2.z));
        }





        















        
        ////////////////////////////////////////// #1 ////////////////////////////////////////////
        // Here we obtain the initial estimation for the scanmatching algorithm
        void updateInitialGuess()
        {
            static Eigen::Affine3f lastImuTransformation;
            static bool lastImuPreTransAvailable = false;
            static Eigen::Affine3f lastImuPreTransformation;
            
            // save current transformation before any processing
            incrementalOdometryAffineFront = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f

            // initialization
            if (data->cloudKeyPoses3D->points.empty())
            {
                transformTobeMapped[0] = cloudInfo.imuRollInit;
                transformTobeMapped[1] = cloudInfo.imuPitchInit;
                transformTobeMapped[2] = cloudInfo.imuYawInit;

                //if (!useImuHeadingInitialization)
                if (!true)
                    transformTobeMapped[2] = 0;

                lastImuTransformation = pcl::getTransformation(0, 0, 0, cloudInfo.imuRollInit, cloudInfo.imuPitchInit, cloudInfo.imuYawInit); // save imu before return;
                return;
            }

            // If Imupreintegration estimate is avaiable
            // use imu pre-integration estimation for scanmatching initial estimate
            //////// HERE IS HOW INFORMATION FROM IMUPREINTEGRATION IS FUSED TO THE MAP OPTIMIZATION.
            //////// IT ONLY USES THE IMUPREINTEGRATION INFORMATION AS AN INITIAL ESTIMATION FOR THE SCANMATCHING PROCESS
            if (cloudInfo.odomAvailable == true)
            {
                Eigen::Affine3f transBack = pcl::getTransformation(cloudInfo.initialGuessX,    cloudInfo.initialGuessY,     cloudInfo.initialGuessZ, 
                                                                   cloudInfo.initialGuessRoll, cloudInfo.initialGuessPitch, cloudInfo.initialGuessYaw);
                if (lastImuPreTransAvailable == false)
                {
                    lastImuPreTransformation = transBack;
                    lastImuPreTransAvailable = true;
                } else {
                    Eigen::Affine3f transIncre = lastImuPreTransformation.inverse() * transBack;
                    Eigen::Affine3f transTobe = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f
                    Eigen::Affine3f transFinal = transTobe * transIncre;
                    pcl::getTranslationAndEulerAngles(transFinal, transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], 
                                                                  transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]);

                    lastImuPreTransformation = transBack;

                    lastImuTransformation = pcl::getTransformation(0, 0, 0, cloudInfo.imuRollInit, cloudInfo.imuPitchInit, cloudInfo.imuYawInit); // save imu before return;
                    return;
                }
            }

            // If Imupreintegration estimate not available,
            // use imu incremental estimation for scanmatching initial estimate (only rotation).
            // Position initial estimation is previous keyframe position
            if (cloudInfo.imuAvailable == true)
            {
                Eigen::Affine3f transBack = pcl::getTransformation(0, 0, 0, cloudInfo.imuRollInit, cloudInfo.imuPitchInit, cloudInfo.imuYawInit);
                Eigen::Affine3f transIncre = lastImuTransformation.inverse() * transBack;

                Eigen::Affine3f transTobe = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f
                Eigen::Affine3f transFinal = transTobe * transIncre;
                // The next line somehow changes transformTobeMapped orientation
                pcl::getTranslationAndEulerAngles(transFinal, transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], 
                                                              transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]);

                lastImuTransformation = pcl::getTransformation(0, 0, 0, cloudInfo.imuRollInit, cloudInfo.imuPitchInit, cloudInfo.imuYawInit); // save imu before return;
                return;
            }
        }
        //////////////////////////////////////// end #1 //////////////////////////////////////////





        ////////////////////////////////////////// #2 ////////////////////////////////////////////
        void extractSurroundingKeyFrames()
        {
            if (data->cloudKeyPoses3D->points.empty() == true)
                return; 
            
            // extractNearby
            pcl::PointCloud<PointType>::Ptr surroundingKeyPoses(new pcl::PointCloud<PointType>());
            pcl::PointCloud<PointType>::Ptr surroundingKeyPosesDS(new pcl::PointCloud<PointType>());
            std::vector<int> pointSearchInd;
            std::vector<float> pointSearchSqDis;

            // extract all the nearby key poses and downsample them
            kdtreeSurroundingKeyPoses->setInputCloud(data->cloudKeyPoses3D); // create kd-tree
            kdtreeSurroundingKeyPoses->radiusSearch(data->cloudKeyPoses3D->back(), (double)50.0, pointSearchInd, pointSearchSqDis); //surroundingKeyframeSearchRadius=50.0
            for (int i = 0; i < (int)pointSearchInd.size(); ++i)
            {
                int id = pointSearchInd[i];
                surroundingKeyPoses->push_back(data->cloudKeyPoses3D->points[id]);
            }

            downSizeFilterSurroundingKeyPoses.setInputCloud(surroundingKeyPoses);
            downSizeFilterSurroundingKeyPoses.filter(*surroundingKeyPosesDS);

            // also extract some latest key frames in case the robot rotates in one position
            for (int i = data->cloudKeyPoses3D->size()-1; i >= 0; --i)
            {
                if (timeLaserInfoCur - data->p6D[i].time < 10.0)
                    surroundingKeyPosesDS->push_back(data->cloudKeyPoses3D->points[i]);
                else
                    break;
            }

            // extractCloud
            // fuse the map
            laserCloudCornerFromMap->clear();
            laserCloudSurfFromMap->clear(); 
            for (int i = 0; i < (int)surroundingKeyPosesDS->size(); ++i)
            {
                if (pointDistance(surroundingKeyPosesDS->points[i], data->cloudKeyPoses3D->back()) > 50.0) //surroundingKeyframeSearchRadius=50.0
                    continue;

                int thisKeyInd = (int)surroundingKeyPosesDS->points[i].intensity;
                if (laserCloudMapContainer.find(thisKeyInd) != laserCloudMapContainer.end()) 
                {
                    // transformed cloud available
                    *laserCloudCornerFromMap += laserCloudMapContainer[thisKeyInd].first;
                    *laserCloudSurfFromMap   += laserCloudMapContainer[thisKeyInd].second;
                } else {
                    // transformed cloud not available
                    pcl::PointCloud<PointType> laserCloudCornerTemp = *transformPointCloud2(data->globalcorner_ptclvec[thisKeyInd],  &data->p6D[thisKeyInd]);
                    pcl::PointCloud<PointType> laserCloudSurfTemp = *transformPointCloud2(data->globalsurf_ptclvec[thisKeyInd],    &data->p6D[thisKeyInd]);
                    *laserCloudCornerFromMap += laserCloudCornerTemp;
                    *laserCloudSurfFromMap   += laserCloudSurfTemp;
                    laserCloudMapContainer[thisKeyInd] = make_pair(laserCloudCornerTemp, laserCloudSurfTemp);
                }
                
            }

            // Downsample the surrounding corner key frames (or map)
            downSizeFilterCorner.setInputCloud(laserCloudCornerFromMap);
            downSizeFilterCorner.filter(*laserCloudCornerFromMapDS);
            laserCloudCornerFromMapDSNum = laserCloudCornerFromMapDS->size();
            // Downsample the surrounding surf key frames (or map)
            downSizeFilterSurf.setInputCloud(laserCloudSurfFromMap);
            downSizeFilterSurf.filter(*laserCloudSurfFromMapDS);
            laserCloudSurfFromMapDSNum = laserCloudSurfFromMapDS->size();

            // clear map cache if too large
            if (laserCloudMapContainer.size() > 1000)
                laserCloudMapContainer.clear();
            
        }
        //////////////////////////////////////// end #2 //////////////////////////////////////////





        ////////////////////////////////////////// #3 ////////////////////////////////////////////
        void downsampleCurrentScan()
        {
            // Downsample cloud from current scan
            laserCloudCornerLastDS->clear();
            downSizeFilterCorner.setInputCloud(laserCloudCornerLast);
            downSizeFilterCorner.filter(*laserCloudCornerLastDS);
            laserCloudCornerLastDSNum = laserCloudCornerLastDS->size();

            laserCloudSurfLastDS->clear();
            downSizeFilterSurf.setInputCloud(laserCloudSurfLast);
            downSizeFilterSurf.filter(*laserCloudSurfLastDS);
            laserCloudSurfLastDSNum = laserCloudSurfLastDS->size();
        }
        //////////////////////////////////////// end #3 //////////////////////////////////////////





        ////////////////////////////////////////// #4 ////////////////////////////////////////////
        void scan2MapOptimization()
        {
            if (data->cloudKeyPoses3D->points.empty())
                return;

            if (laserCloudCornerLastDSNum > 10 && laserCloudSurfLastDSNum > 100)
            {
                kdtreeCornerFromMap->setInputCloud(laserCloudCornerFromMapDS);
                kdtreeSurfFromMap->setInputCloud(laserCloudSurfFromMapDS);

                for (int iterCount = 0; iterCount < 30; iterCount++)
                {
                    laserCloudOri->clear();
                    coeffSel->clear();

                    cornerOptimization();
                    surfOptimization();

                    combineOptimizationCoeffs();

                    if (LMOptimization(iterCount) == true)
                        break;              
                }

                transformUpdate();
            } else {
                ROS_WARN("Not enough features! Only %d edge and %d planar features available.", laserCloudCornerLastDSNum, laserCloudSurfLastDSNum);
            }
        }

        void cornerOptimization()
        {
            transPointAssociateToMap = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f // update Point Associate To Map

            // numberOfCores=4
            #pragma omp parallel for num_threads(4)
            for (int i = 0; i < laserCloudCornerLastDSNum; i++)
            {
                PointType pointOri, pointSel, coeff;
                std::vector<int> pointSearchInd;
                std::vector<float> pointSearchSqDis;

                pointOri = laserCloudCornerLastDS->points[i];

                // pointAssociateToMap 
                pointSel.x = transPointAssociateToMap(0,0) * pointOri.x + transPointAssociateToMap(0,1) * pointOri.y + transPointAssociateToMap(0,2) * pointOri.z + transPointAssociateToMap(0,3);
                pointSel.y = transPointAssociateToMap(1,0) * pointOri.x + transPointAssociateToMap(1,1) * pointOri.y + transPointAssociateToMap(1,2) * pointOri.z + transPointAssociateToMap(1,3);
                pointSel.z = transPointAssociateToMap(2,0) * pointOri.x + transPointAssociateToMap(2,1) * pointOri.y + transPointAssociateToMap(2,2) * pointOri.z + transPointAssociateToMap(2,3);
                pointSel.intensity = pointOri.intensity;

                kdtreeCornerFromMap->nearestKSearch(pointSel, 5, pointSearchInd, pointSearchSqDis);

                cv::Mat matA1(3, 3, CV_32F, cv::Scalar::all(0));
                cv::Mat matD1(1, 3, CV_32F, cv::Scalar::all(0));
                cv::Mat matV1(3, 3, CV_32F, cv::Scalar::all(0));
                        
                if (pointSearchSqDis[4] < 1.0) {
                    float cx = 0, cy = 0, cz = 0;
                    for (int j = 0; j < 5; j++) {
                        cx += laserCloudCornerFromMapDS->points[pointSearchInd[j]].x;
                        cy += laserCloudCornerFromMapDS->points[pointSearchInd[j]].y;
                        cz += laserCloudCornerFromMapDS->points[pointSearchInd[j]].z;
                    }
                    cx /= 5; cy /= 5;  cz /= 5;

                    float a11 = 0, a12 = 0, a13 = 0, a22 = 0, a23 = 0, a33 = 0;
                    for (int j = 0; j < 5; j++) {
                        float ax = laserCloudCornerFromMapDS->points[pointSearchInd[j]].x - cx;
                        float ay = laserCloudCornerFromMapDS->points[pointSearchInd[j]].y - cy;
                        float az = laserCloudCornerFromMapDS->points[pointSearchInd[j]].z - cz;

                        a11 += ax * ax; a12 += ax * ay; a13 += ax * az;
                        a22 += ay * ay; a23 += ay * az;
                        a33 += az * az;
                    }
                    a11 /= 5; a12 /= 5; a13 /= 5; a22 /= 5; a23 /= 5; a33 /= 5;

                    matA1.at<float>(0, 0) = a11; matA1.at<float>(0, 1) = a12; matA1.at<float>(0, 2) = a13;
                    matA1.at<float>(1, 0) = a12; matA1.at<float>(1, 1) = a22; matA1.at<float>(1, 2) = a23;
                    matA1.at<float>(2, 0) = a13; matA1.at<float>(2, 1) = a23; matA1.at<float>(2, 2) = a33;

                    cv::eigen(matA1, matD1, matV1);

                    if (matD1.at<float>(0, 0) > 3 * matD1.at<float>(0, 1)) {

                        float x0 = pointSel.x;
                        float y0 = pointSel.y;
                        float z0 = pointSel.z;
                        float x1 = cx + 0.1 * matV1.at<float>(0, 0);
                        float y1 = cy + 0.1 * matV1.at<float>(0, 1);
                        float z1 = cz + 0.1 * matV1.at<float>(0, 2);
                        float x2 = cx - 0.1 * matV1.at<float>(0, 0);
                        float y2 = cy - 0.1 * matV1.at<float>(0, 1);
                        float z2 = cz - 0.1 * matV1.at<float>(0, 2);

                        float a012 = sqrt(((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1)) * ((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1)) 
                                        + ((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1)) * ((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1)) 
                                        + ((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1)) * ((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1)));

                        float l12 = sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2) + (z1 - z2)*(z1 - z2));

                        float la = ((y1 - y2)*((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1)) 
                                  + (z1 - z2)*((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1))) / a012 / l12;

                        float lb = -((x1 - x2)*((x0 - x1)*(y0 - y2) - (x0 - x2)*(y0 - y1)) 
                                   - (z1 - z2)*((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1))) / a012 / l12;

                        float lc = -((x1 - x2)*((x0 - x1)*(z0 - z2) - (x0 - x2)*(z0 - z1)) 
                                   + (y1 - y2)*((y0 - y1)*(z0 - z2) - (y0 - y2)*(z0 - z1))) / a012 / l12;

                        float ld2 = a012 / l12;

                        float s = 1 - 0.9 * fabs(ld2);

                        coeff.x = s * la;
                        coeff.y = s * lb;
                        coeff.z = s * lc;
                        coeff.intensity = s * ld2;

                        if (s > 0.1) {
                            laserCloudOriCornerVec[i] = pointOri;
                            coeffSelCornerVec[i] = coeff;
                            laserCloudOriCornerFlag[i] = true;
                        }
                    }
                }
            }
        }

        void surfOptimization()
        {
            transPointAssociateToMap = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f // update Point Associate To Map

            // numberOfCores=4
            #pragma omp parallel for num_threads(4)
            for (int i = 0; i < laserCloudSurfLastDSNum; i++)
            {
                PointType pointOri, pointSel, coeff;
                std::vector<int> pointSearchInd;
                std::vector<float> pointSearchSqDis;

                pointOri = laserCloudSurfLastDS->points[i];

                // pointAssociateToMap 
                pointSel.x = transPointAssociateToMap(0,0) * pointOri.x + transPointAssociateToMap(0,1) * pointOri.y + transPointAssociateToMap(0,2) * pointOri.z + transPointAssociateToMap(0,3);
                pointSel.y = transPointAssociateToMap(1,0) * pointOri.x + transPointAssociateToMap(1,1) * pointOri.y + transPointAssociateToMap(1,2) * pointOri.z + transPointAssociateToMap(1,3);
                pointSel.z = transPointAssociateToMap(2,0) * pointOri.x + transPointAssociateToMap(2,1) * pointOri.y + transPointAssociateToMap(2,2) * pointOri.z + transPointAssociateToMap(2,3);
                pointSel.intensity = pointOri.intensity;

                kdtreeSurfFromMap->nearestKSearch(pointSel, 5, pointSearchInd, pointSearchSqDis);

                Eigen::Matrix<float, 5, 3> matA0;
                Eigen::Matrix<float, 5, 1> matB0;
                Eigen::Vector3f matX0;

                matA0.setZero();
                matB0.fill(-1);
                matX0.setZero();

                if (pointSearchSqDis[4] < 1.0) {
                    for (int j = 0; j < 5; j++) {
                        matA0(j, 0) = laserCloudSurfFromMapDS->points[pointSearchInd[j]].x;
                        matA0(j, 1) = laserCloudSurfFromMapDS->points[pointSearchInd[j]].y;
                        matA0(j, 2) = laserCloudSurfFromMapDS->points[pointSearchInd[j]].z;
                    }

                    matX0 = matA0.colPivHouseholderQr().solve(matB0);

                    float pa = matX0(0, 0);
                    float pb = matX0(1, 0);
                    float pc = matX0(2, 0);
                    float pd = 1;

                    float ps = sqrt(pa * pa + pb * pb + pc * pc);
                    pa /= ps; pb /= ps; pc /= ps; pd /= ps;

                    bool planeValid = true;
                    for (int j = 0; j < 5; j++) {
                        if (fabs(pa * laserCloudSurfFromMapDS->points[pointSearchInd[j]].x +
                                 pb * laserCloudSurfFromMapDS->points[pointSearchInd[j]].y +
                                 pc * laserCloudSurfFromMapDS->points[pointSearchInd[j]].z + pd) > 0.2) {
                            planeValid = false;
                            break;
                        }
                    }

                    if (planeValid) {
                        float pd2 = pa * pointSel.x + pb * pointSel.y + pc * pointSel.z + pd;

                        float s = 1 - 0.9 * fabs(pd2) / sqrt(sqrt(pointSel.x * pointSel.x
                                + pointSel.y * pointSel.y + pointSel.z * pointSel.z));

                        coeff.x = s * pa;
                        coeff.y = s * pb;
                        coeff.z = s * pc;
                        coeff.intensity = s * pd2;

                        if (s > 0.1) {
                            laserCloudOriSurfVec[i] = pointOri;
                            coeffSelSurfVec[i] = coeff;
                            laserCloudOriSurfFlag[i] = true;
                        }
                    }
                }
            }
        }



        void combineOptimizationCoeffs()
        {
            // combine corner coeffs
            for (int i = 0; i < laserCloudCornerLastDSNum; ++i){
                if (laserCloudOriCornerFlag[i] == true){
                    laserCloudOri->push_back(laserCloudOriCornerVec[i]);
                    coeffSel->push_back(coeffSelCornerVec[i]);
                }
            }
            // combine surf coeffs
            for (int i = 0; i < laserCloudSurfLastDSNum; ++i){
                if (laserCloudOriSurfFlag[i] == true){
                    laserCloudOri->push_back(laserCloudOriSurfVec[i]);
                    coeffSel->push_back(coeffSelSurfVec[i]);
                }
            }
            // reset flag for next iteration
            std::fill(laserCloudOriCornerFlag.begin(), laserCloudOriCornerFlag.end(), false);
            std::fill(laserCloudOriSurfFlag.begin(), laserCloudOriSurfFlag.end(), false);
        }

        bool LMOptimization(int iterCount)
        {
            // This optimization is from the original loam_velodyne by Ji Zhang, need to cope with coordinate transformation
            // lidar <- camera      ---     camera <- lidar
            // x = z                ---     x = y
            // y = x                ---     y = z
            // z = y                ---     z = x
            // roll = yaw           ---     roll = pitch
            // pitch = roll         ---     pitch = yaw
            // yaw = pitch          ---     yaw = roll

            // lidar -> camera
            float srx = sin(transformTobeMapped[1]);
            float crx = cos(transformTobeMapped[1]);
            float sry = sin(transformTobeMapped[2]);
            float cry = cos(transformTobeMapped[2]);
            float srz = sin(transformTobeMapped[0]);
            float crz = cos(transformTobeMapped[0]);

            int laserCloudSelNum = laserCloudOri->size();
            if (laserCloudSelNum < 50) {
                return false;
            }

            cv::Mat matA(laserCloudSelNum, 6, CV_32F, cv::Scalar::all(0));
            cv::Mat matAt(6, laserCloudSelNum, CV_32F, cv::Scalar::all(0));
            cv::Mat matAtA(6, 6, CV_32F, cv::Scalar::all(0));
            cv::Mat matB(laserCloudSelNum, 1, CV_32F, cv::Scalar::all(0));
            cv::Mat matAtB(6, 1, CV_32F, cv::Scalar::all(0));
            cv::Mat matX(6, 1, CV_32F, cv::Scalar::all(0));

            PointType pointOri, coeff;

            for (int i = 0; i < laserCloudSelNum; i++) {
                // lidar -> camera
                pointOri.x = laserCloudOri->points[i].y;
                pointOri.y = laserCloudOri->points[i].z;
                pointOri.z = laserCloudOri->points[i].x;
                // lidar -> camera
                coeff.x = coeffSel->points[i].y;
                coeff.y = coeffSel->points[i].z;
                coeff.z = coeffSel->points[i].x;
                coeff.intensity = coeffSel->points[i].intensity;
                // in camera
                float arx = (crx*sry*srz*pointOri.x + crx*crz*sry*pointOri.y - srx*sry*pointOri.z) * coeff.x
                          + (-srx*srz*pointOri.x - crz*srx*pointOri.y - crx*pointOri.z) * coeff.y
                          + (crx*cry*srz*pointOri.x + crx*cry*crz*pointOri.y - cry*srx*pointOri.z) * coeff.z;

                float ary = ((cry*srx*srz - crz*sry)*pointOri.x 
                          + (sry*srz + cry*crz*srx)*pointOri.y + crx*cry*pointOri.z) * coeff.x
                          + ((-cry*crz - srx*sry*srz)*pointOri.x 
                          + (cry*srz - crz*srx*sry)*pointOri.y - crx*sry*pointOri.z) * coeff.z;

                float arz = ((crz*srx*sry - cry*srz)*pointOri.x + (-cry*crz-srx*sry*srz)*pointOri.y)*coeff.x
                          + (crx*crz*pointOri.x - crx*srz*pointOri.y) * coeff.y
                          + ((sry*srz + cry*crz*srx)*pointOri.x + (crz*sry-cry*srx*srz)*pointOri.y)*coeff.z;
                // lidar -> camera
                matA.at<float>(i, 0) = arz;
                matA.at<float>(i, 1) = arx;
                matA.at<float>(i, 2) = ary;
                matA.at<float>(i, 3) = coeff.z;
                matA.at<float>(i, 4) = coeff.x;
                matA.at<float>(i, 5) = coeff.y;
                matB.at<float>(i, 0) = -coeff.intensity;
            }

            cv::transpose(matA, matAt);
            matAtA = matAt * matA;
            matAtB = matAt * matB;
            cv::solve(matAtA, matAtB, matX, cv::DECOMP_QR);

            if (iterCount == 0) {

                cv::Mat matE(1, 6, CV_32F, cv::Scalar::all(0));
                cv::Mat matV(6, 6, CV_32F, cv::Scalar::all(0));
                cv::Mat matV2(6, 6, CV_32F, cv::Scalar::all(0));

                cv::eigen(matAtA, matE, matV);
                matV.copyTo(matV2);

                isDegenerate = false;
                float eignThre[6] = {100, 100, 100, 100, 100, 100};
                for (int i = 5; i >= 0; i--) {
                    if (matE.at<float>(0, i) < eignThre[i]) {
                        for (int j = 0; j < 6; j++) {
                            matV2.at<float>(i, j) = 0;
                        }
                        isDegenerate = true;
                    } else {
                        break;
                    }
                }
                matP = matV.inv() * matV2;
            }

            if (isDegenerate)
            {
                cv::Mat matX2(6, 1, CV_32F, cv::Scalar::all(0));
                matX.copyTo(matX2);
                matX = matP * matX2;
            }

            transformTobeMapped[0] += matX.at<float>(0, 0);
            transformTobeMapped[1] += matX.at<float>(1, 0);
            transformTobeMapped[2] += matX.at<float>(2, 0);
            transformTobeMapped[3] += matX.at<float>(3, 0);
            transformTobeMapped[4] += matX.at<float>(4, 0);
            transformTobeMapped[5] += matX.at<float>(5, 0);

            float deltaR = sqrt(
                                pow(pcl::rad2deg(matX.at<float>(0, 0)), 2) +
                                pow(pcl::rad2deg(matX.at<float>(1, 0)), 2) +
                                pow(pcl::rad2deg(matX.at<float>(2, 0)), 2));
            float deltaT = sqrt(
                                pow(matX.at<float>(3, 0) * 100, 2) +
                                pow(matX.at<float>(4, 0) * 100, 2) +
                                pow(matX.at<float>(5, 0) * 100, 2));

            if (deltaR < 0.05 && deltaT < 0.05) {
                return true; // converged
            }
            return false; // keep optimizing
        }

        void transformUpdate()
        {
            if (cloudInfo.imuAvailable == true)
            {
                if (std::abs(cloudInfo.imuPitchInit) < 1.4)
                {
                    double imuWeight = 0.01;
                    tf::Quaternion imuQuaternion;
                    tf::Quaternion transformQuaternion;
                    double rollMid, pitchMid, yawMid;

                    // slerp roll
                    transformQuaternion.setRPY(transformTobeMapped[0], 0, 0);
                    imuQuaternion.setRPY(cloudInfo.imuRollInit, 0, 0);
                    tf::Matrix3x3(transformQuaternion.slerp(imuQuaternion, imuWeight)).getRPY(rollMid, pitchMid, yawMid);
                    transformTobeMapped[0] = rollMid;

                    // slerp pitch
                    transformQuaternion.setRPY(0, transformTobeMapped[1], 0);
                    imuQuaternion.setRPY(0, cloudInfo.imuPitchInit, 0);
                    tf::Matrix3x3(transformQuaternion.slerp(imuQuaternion, imuWeight)).getRPY(rollMid, pitchMid, yawMid);
                    transformTobeMapped[1] = pitchMid;
                }
            }

            transformTobeMapped[0] = constraintTransformation(transformTobeMapped[0], 1000); //rotation_tollerance=1000
            transformTobeMapped[1] = constraintTransformation(transformTobeMapped[1], 1000); //rotation_tollerance=1000
            transformTobeMapped[5] = constraintTransformation(transformTobeMapped[5], 1000); //z_tollerance=1000

            incrementalOdometryAffineBack = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f
        }

        float constraintTransformation(float value, float limit)
        {
            if (value < -limit)
                value = -limit;
            if (value > limit)
                value = limit;

            return value;
        }
        //////////////////////////////////////// end #4 //////////////////////////////////////////






        ////////////////////////////////////////// #5 ////////////////////////////////////////////
        void saveKeyFramesAndFactor()
        {
            // We check here if the scan-matching has detected a big enough movement to add the node
            if (saveFrame() == false){
                return;
            }
            //std::cout << "Scan accepted" << std::endl;
            //std::cout << "ACCEPTED: " << transformTobeMapped[0] << " " << transformTobeMapped[1] << " "<< transformTobeMapped[2] << " "<< transformTobeMapped[3] << " "<< transformTobeMapped[4] << " "<< transformTobeMapped[5] << std::endl;

            // odom factor
            addOdomFactor();

            // gps factor
            addGPSFactor();

            // loop factor
            // addLoopFactor();

            // cout << "****************************************************" << endl;
            // gtSAMgraph.print("GTSAM Graph:\n");

            data->optimize_flag = true;

        }

        /* Did the scan matching detected a big enough movement to add a node?
         */
        bool saveFrame()
        {
            if (data->cloudKeyPoses3D->points.empty())
                return true;

            Eigen::Affine3f transStart = pcl::getTransformation(data->p6D.back().x, data->p6D.back().y, data->p6D.back().z, data->p6D.back().roll, data->p6D.back().pitch, data->p6D.back().yaw); // pcl Point To Affine3f
            // transformTobeMapped is T_odom^new_node given by scanmatching
            Eigen::Affine3f transFinal = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], 
                                                                transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]);
            Eigen::Affine3f transBetween = transStart.inverse() * transFinal;
            float x, y, z, roll, pitch, yaw;
            pcl::getTranslationAndEulerAngles(transBetween, x, y, z, roll, pitch, yaw);

            if (abs(roll)  < 0.2 &&
                abs(pitch) < 0.2 && 
                abs(yaw)   < 0.2 &&
                sqrt(x*x + y*y + z*z) < 0.2) //surroundingkeyframeAddingAngleThreshold=0.2
                return false;

            return true;
        }


        // Scanmatching constraint is stored
        // KEYFRAME IS NOT STORED HERE
        void addOdomFactor()
        {
            // transformTobeMapped is T_odom^new_node given by scanmatching
            gtsam::Pose3 transformTobeMappedPose3 = gtsam::Pose3(gtsam::Rot3::RzRyRx(transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]), 
                                                                 gtsam::Point3(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5])); // trans to gtsam Pose3
            
            // If it's the first node, just add a prior constraint on the first node
            if (data->cloudKeyPoses3D->points.empty()){
                gtsam::noiseModel::Diagonal::shared_ptr priorNoise = gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 1e-2, 1e-2, M_PI*M_PI, 1e8, 1e8, 1e8).finished()); // rad*rad, meter*meter
                gtsam::PriorFactor<gtsam::Pose3> prior( 0, transformTobeMappedPose3, priorNoise); // At index=0

                data->gtSAMgraph.add(prior);
                data->initialEstimate.insert(0, transformTobeMappedPose3); // at index=0
            }

            else{
                gtsam::Pose3 poseFrom = gtsam::Pose3(gtsam::Rot3::RzRyRx(data->p6D.back().roll, data->p6D.back().pitch, data->p6D.back().yaw), 
                                                     gtsam::Point3(data->p6D.back().x, data->p6D.back().y, data->p6D.back().z)); //pcl Point To gtsam Pose3
                gtsam::Pose3 poseTo   = transformTobeMappedPose3;
                gtsam::noiseModel::Diagonal::shared_ptr odometryNoise = gtsam::noiseModel::Diagonal::Variances((gtsam::Vector(6) << 1e-6, 1e-6, 1e-6, 1e-4, 1e-4, 1e-4).finished()); // Scan-Matching constraints constant covariance
                //						index previous node        index current node       transform                 covariance
                gtsam::BetweenFactor<gtsam::Pose3> scanMatchFactor(data->cloudKeyPoses3D->size()-1, data->cloudKeyPoses3D->size(), poseFrom.between(poseTo), odometryNoise);

                data->gtSAMgraph.add(scanMatchFactor);
                //std::cout << "---------" << std::endl;
                //std::cout << cloudKeyPoses3D->size() << std::endl;
                //std::cout << isam->calculateEstimate().size() << std::endl;
                //std::cout << "---------" << std::endl;
                //                     index                    initial estimate
                data->initialEstimate.insert(data->cloudKeyPoses3D->size(), poseTo);
            }
        }


        // Each time a node is added, we pop the front up to a gps that if close to the keyframe time
        void addGPSFactor()
        {
            if (data->gpsQueue.empty())
            {
                return;
            }

            // wait for system initialized and settles down
            if (data->cloudKeyPoses3D->points.empty())
            {
                return;
            }

            // if the current keyframe is too close to the first keyframe
            else if (pointDistance(data->cloudKeyPoses3D->front(), data->cloudKeyPoses3D->back()) < 5.0)
            { 
                //std::cout << "Point Distance" << std::endl;
                return;
            }

            // pose covariance small, no need to correct
            /*
            if (poseCovariance(3,3) < poseCovThreshold && poseCovariance(4,4) < poseCovThreshold){
                std::cout << "here2" << std::endl;
                return;
            }
            */

            // last gps position
            //static PointType lastGPSPoint;

            // There might be multiple gps data 
            //std::cout << "-----------" << std::endl;
            while (!data->gpsQueue.empty())
            {

                nav_msgs::Odometry thisGPS = data->gpsQueue.front();
                float gps_x = thisGPS.pose.pose.position.x;
                float gps_y = thisGPS.pose.pose.position.y;
                float gps_z = thisGPS.pose.pose.position.z;
                float noise_x = thisGPS.pose.covariance[0];
                float noise_y = thisGPS.pose.covariance[7];
                float noise_z = thisGPS.pose.covariance[14];
                /* 
                if (!useGpsElevation)
                {
                    gps_z = transformTobeMapped[5];
                    noise_z = 0.01;
                }
                */

                if (thisGPS.header.stamp.toSec() < timeLaserInfoCur - 0.2)
                {
                    // message too old
                    data->gpsQueue.pop_front();
                    //std::cout << "Too old" << std::endl;
                }
                else if (thisGPS.header.stamp.toSec() > timeLaserInfoCur + 0.2)
                {
                    // message too new
                    //std::cout << "Too new" << std::endl;
                    break;
                }
                else if (noise_x > 2.0 || noise_y > 2.0)
                {
                    // GPS too noisy, skip
                    //std::cout << "Too noisy" << std::endl;
                    data->gpsQueue.pop_front();
                }
                else if (abs(gps_x) < 1e-6 && abs(gps_y) < 1e-6) // This is specific to your GPS receiver
                {
                    // GPS not properly initialized (0,0,0)
                    //std::cout << "GPS (0,0,0)" << std::endl;
                    data->gpsQueue.pop_front();
                }
                else
                {

                    // Add GPS every a few meters
                    /*
                    PointType curGPSPoint;
                    curGPSPoint.x = gps_x;
                    curGPSPoint.y = gps_y;
                    curGPSPoint.z = gps_z;
                    if (pointDistance(curGPSPoint, lastGPSPoint) < 5.0){
                        std::cout << "Too close" << std::endl;
                        continue;
                    }
                    else
                        lastGPSPoint = curGPSPoint;
                    */

                    data->gpsQueue.pop_front();
                    //std::cout << "Accepted" << std::endl;
                    gtsam::Vector Vector3(3);
                    Vector3 << min(noise_x, 1.0f), min(noise_y, 1.0f), min(noise_z, 1.0f); // so the minimum noise is always 1.0
                    gtsam::noiseModel::Diagonal::shared_ptr gps_noise = gtsam::noiseModel::Diagonal::Variances(Vector3);
                    gtsam::GPSFactor gps_factor(data->cloudKeyPoses3D->size(), gtsam::Point3(gps_x, gps_y, gps_z), gps_noise);
                    data->gtSAMgraph.add(gps_factor);

                    data->aLoopIsClosed = true;
                    break;
                }
            }
            //std::cout << "-----------" << std::endl;
        }
        //////////////////////////////////////// end #5 //////////////////////////////////////////
    





        //////////////////////////////////////// end #6 //////////////////////////////////////////
        void optimizeGraph()
        {

            if (data->optimize_flag == false){
                return;
            }
            // update iSAM
            data->graph_mutex.lock();
            gtsam::NonlinearFactorGraph graphtmp = data->gtSAMgraph;
            gtsam::Values initialtmp = data->initialEstimate;
            data->gtSAMgraph.resize(0);
            data->initialEstimate.clear();
            data->optimize_flag = false;
            data->graph_mutex.unlock();
            
            data->isam->update(graphtmp, initialtmp);
            data->isam->update();
            // Why call update that many times?
            if (data->aLoopIsClosed == true)
            {
                data->isam->update();
                data->isam->update();
                data->isam->update();
                data->isam->update();
                data->isam->update();
            }
           


            /////////////////////////////// STORE NEW POSE, PATH AND POINT CLOUDS ///////////////////////////////
            // Here, we update
            // 		-cloudKeyPoses3D
            // 		-cloudKeyPoses6D
            // 		-transformTobeMapped
            // 		-globalcorner_ptclvec
            // 		-suftCloudKeyFrames
            //save key poses
            PointType tmpPose3D;
            pose6D tmpPose;

            // cout << "****************************************************" << endl;
            // isam->calculateEstimate().print("Current estimate: ");

            tmpPose3D.x = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().x();
            tmpPose3D.y = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().y();
            tmpPose3D.z = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().z();
            tmpPose3D.intensity = data->isam->calculateEstimate().size()-1; // this can be used as index
            data->cloudKeyPoses3D->push_back(tmpPose3D);


            tmpPose.x = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().x();
            tmpPose.y = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().y();
            tmpPose.z = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().z();
            tmpPose.idx = data->isam->calculateEstimate().size()-1; // this can be used as index
            tmpPose.roll  = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().roll();
            tmpPose.pitch = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().pitch();
            tmpPose.yaw   = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().yaw();
            tmpPose.time = timeLaserInfoCur;
            data->p6D.push_back(tmpPose);

            // cout << "****************************************************" << endltL
            // cout << "Pose covariance:" << endl;
            // cout << isam->marginalCovariance(isam->calculateEstimate().size()-1) << endl << endl;
            data->poseCovariance = data->isam->marginalCovariance(data->isam->calculateEstimate().size()-1);

            // save updated transform
            transformTobeMapped[0] = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().roll();
            transformTobeMapped[1] = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().pitch();
            transformTobeMapped[2] = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().yaw();
            transformTobeMapped[3] = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().x();
            transformTobeMapped[4] = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().y();
            transformTobeMapped[5] = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().z();
            // transformTobeMapped is now T_odom^new_node given by optimization (should equal scanmatching estimate if no gps or loopclosure is attached to new node)
            //std::cout << "UPDATED: " << transformTobeMapped[0] << " " << transformTobeMapped[1] << " "<< transformTobeMapped[2] << " "<< transformTobeMapped[3] << " "<< transformTobeMapped[4] << " "<< transformTobeMapped[5] << std::endl;

            // save downsampled corner and surface point cloud of this new node
            pcl::PointCloud<PointType>::Ptr thisCornerKeyFrame(new pcl::PointCloud<PointType>());
            pcl::PointCloud<PointType>::Ptr thisSurfKeyFrame(new pcl::PointCloud<PointType>());
            pcl::copyPointCloud(*laserCloudCornerLastDS,  *thisCornerKeyFrame);// Copy this way since the point cloud vector needs a ptr
            pcl::copyPointCloud(*laserCloudSurfLastDS,    *thisSurfKeyFrame);
            data->globalcorner_ptclvec.push_back(thisCornerKeyFrame);
            data->globalsurf_ptclvec.push_back(thisSurfKeyFrame);
            

            // save path for visualization
            // if only a scan matching constraint has been added, then only the current pose needs to be updated for visualization
            if (data->aLoopIsClosed == false)
            {
                geometry_msgs::PoseStamped pose_stamped;
                pose_stamped.header.stamp = ros::Time().fromSec(timeLaserInfoCur);
                pose_stamped.header.frame_id = "odom";
                pose_stamped.pose.position.x = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().x();
                pose_stamped.pose.position.y = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().y();
                pose_stamped.pose.position.z = data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).translation().z();
                tf::Quaternion q = tf::createQuaternionFromRPY(data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().roll(),
                                                           data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().pitch(),
                                                           data->isam->calculateEstimate().at<gtsam::Pose3>(data->isam->calculateEstimate().size()-1).rotation().yaw());
                pose_stamped.pose.orientation.x = q.x();
                pose_stamped.pose.orientation.y = q.y();
                pose_stamped.pose.orientation.z = q.z();
                pose_stamped.pose.orientation.w = q.w();

                data->globalPath.poses.push_back(pose_stamped);
            }
            else // if a gps or loopclosure constraint has been added, then we need to update the full path for visuzalisation
            {
                // clear map cache
                laserCloudMapContainer.clear();
                // clear path
                data->globalPath.poses.clear();
                // update key poses
                for (unsigned int i = 0; i < data->isam->calculateEstimate().size(); ++i)
                {
                    data->cloudKeyPoses3D->points[i].x = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().x();
                    data->cloudKeyPoses3D->points[i].y = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().y();
                    data->cloudKeyPoses3D->points[i].z = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().z();


                    data->p6D[i].x = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().x();
                    data->p6D[i].y = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().y();
                    data->p6D[i].z = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().z();
                    data->p6D[i].roll  = data->isam->calculateEstimate().at<gtsam::Pose3>(i).rotation().roll();
                    data->p6D[i].pitch = data->isam->calculateEstimate().at<gtsam::Pose3>(i).rotation().pitch();
                    data->p6D[i].yaw   = data->isam->calculateEstimate().at<gtsam::Pose3>(i).rotation().yaw();

                    geometry_msgs::PoseStamped pose_stamped;
                    pose_stamped.header.stamp = ros::Time().fromSec(data->p6D[i].time);
                    pose_stamped.header.frame_id = "odom";
                    pose_stamped.pose.position.x = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().x();
                    pose_stamped.pose.position.y = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().y();
                    pose_stamped.pose.position.z = data->isam->calculateEstimate().at<gtsam::Pose3>(i).translation().z();
                    tf::Quaternion q = tf::createQuaternionFromRPY(data->isam->calculateEstimate().at<gtsam::Pose3>(i).rotation().roll(),
                                                                   data->isam->calculateEstimate().at<gtsam::Pose3>(i).rotation().pitch(),
                                                                   data->isam->calculateEstimate().at<gtsam::Pose3>(i).rotation().yaw());
                    pose_stamped.pose.orientation.x = q.x();
                    pose_stamped.pose.orientation.y = q.y();
                    pose_stamped.pose.orientation.z = q.z();
                    pose_stamped.pose.orientation.w = q.w();

                    data->globalPath.poses.push_back(pose_stamped);
                }

                data->aLoopIsClosed = false;
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////
        }


        //////////////////////////////////////// end #6 //////////////////////////////////////////








        ////////////////////////////////////////// #7 ////////////////////////////////////////////
        // transformTobeMapped is T_odom^new_node given by optimization (should equal scanmatching estimate if no gps or loopclosure is attached to new node)
        void publishData()
        {
            // Publish ALL THE SCAN MATCHING POSE (accepted or not accepted) on /lio_sam/mapping/odometry
            nav_msgs::Odometry laserOdometryROS;
            laserOdometryROS.header.stamp = timeLaserInfoStamp;
            laserOdometryROS.header.frame_id = "odom";
            laserOdometryROS.child_frame_id = "odom_mapping";
            laserOdometryROS.pose.pose.position.x = transformTobeMapped[3];
            laserOdometryROS.pose.pose.position.y = transformTobeMapped[4];
            laserOdometryROS.pose.pose.position.z = transformTobeMapped[5];
            laserOdometryROS.pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]);
            pubLaserOdometryGlobal.publish(laserOdometryROS);

            //std::cout << "PUBLISH: " << transformTobeMapped[0] << " " << transformTobeMapped[1] << " "<< transformTobeMapped[2] << " "<< transformTobeMapped[3] << " "<< transformTobeMapped[4] << " "<< transformTobeMapped[5] << std::endl;
            //std::cout << "PublishData" << std::endl;
            
            // Publish ALL THE SCAN MATCHING POSE (accepted or not accepted) on tf as odom->base_link
            static tf::TransformBroadcaster br;
            tf::Transform t_odom_to_lidar = tf::Transform(tf::createQuaternionFromRPY(transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]),
                                                          tf::Vector3(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5]));
            tf::StampedTransform trans_odom_to_lidar = tf::StampedTransform(t_odom_to_lidar, timeLaserInfoStamp, "odom", "base_link");
            br.sendTransform(trans_odom_to_lidar);

            // Publish odometry for ROS (incremental)
            static bool lastIncreOdomPubFlag = false;
            static nav_msgs::Odometry laserOdomIncremental; // incremental odometry msg
            static Eigen::Affine3f increOdomAffine; // incremental odometry in affine
            if (lastIncreOdomPubFlag == false)
            {
                //std::cout << "false" << std::endl;
                lastIncreOdomPubFlag = true;
                laserOdomIncremental = laserOdometryROS;
                increOdomAffine = pcl::getTransformation(transformTobeMapped[3], transformTobeMapped[4], transformTobeMapped[5], transformTobeMapped[0], transformTobeMapped[1], transformTobeMapped[2]); // trans 2 Affine3f
            } 
            else 
            {
                //std::cout << "true" << std::endl;
                Eigen::Affine3f affineIncre = incrementalOdometryAffineFront.inverse() * incrementalOdometryAffineBack;
                increOdomAffine = increOdomAffine * affineIncre;
                float x, y, z, roll, pitch, yaw;
                pcl::getTranslationAndEulerAngles (increOdomAffine, x, y, z, roll, pitch, yaw);
                if (cloudInfo.imuAvailable == true)
                {
                    if (std::abs(cloudInfo.imuPitchInit) < 1.4)
                    {
                        double imuWeight = 0.1;
                        tf::Quaternion imuQuaternion;
                        tf::Quaternion transformQuaternion;
                        double rollMid, pitchMid, yawMid;

                        // slerp roll
                        transformQuaternion.setRPY(roll, 0, 0);
                        imuQuaternion.setRPY(cloudInfo.imuRollInit, 0, 0);
                        tf::Matrix3x3(transformQuaternion.slerp(imuQuaternion, imuWeight)).getRPY(rollMid, pitchMid, yawMid);
                        roll = rollMid;

                        // slerp pitch
                        transformQuaternion.setRPY(0, pitch, 0);
                        imuQuaternion.setRPY(0, cloudInfo.imuPitchInit, 0);
                        tf::Matrix3x3(transformQuaternion.slerp(imuQuaternion, imuWeight)).getRPY(rollMid, pitchMid, yawMid);
                        pitch = pitchMid;
                    }
                }
                laserOdomIncremental.header.stamp = timeLaserInfoStamp;
                laserOdomIncremental.header.frame_id = "odom";
                laserOdomIncremental.child_frame_id = "odom_mapping";
                laserOdomIncremental.pose.pose.position.x = x;
                laserOdomIncremental.pose.pose.position.y = y;
                laserOdomIncremental.pose.pose.position.z = z;
                laserOdomIncremental.pose.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(roll, pitch, yaw);
                if (isDegenerate)
                    laserOdomIncremental.pose.covariance[0] = 1;
                else
                    laserOdomIncremental.pose.covariance[0] = 0;
            }
            pubLaserOdometryIncremental.publish(laserOdomIncremental);

            //// Publish frames ////
            if (data->cloudKeyPoses3D->points.empty())
                return;
            // publish key poses
            publishCloud(&pubKeyPoses, data->cloudKeyPoses3D, timeLaserInfoStamp, "odom");
            // Publish surrounding key frames
            publishCloud(&pubRecentKeyFrames, laserCloudSurfFromMapDS, timeLaserInfoStamp, "odom");
            // publish registered key frame
            if (pubRecentKeyFrame.getNumSubscribers() != 0)
            {
                pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());
                pose6D p = trans2pose6D(transformTobeMapped);
                *cloudOut += *transformPointCloud2(laserCloudCornerLastDS,  &p);
                *cloudOut += *transformPointCloud2(laserCloudSurfLastDS,    &p);
                publishCloud(&pubRecentKeyFrame, cloudOut, timeLaserInfoStamp, "odom");
            }
            // publish registered high-res raw cloud
            if (pubCloudRegisteredRaw.getNumSubscribers() != 0)
            {
                pcl::PointCloud<PointType>::Ptr cloudOut(new pcl::PointCloud<PointType>());
                pcl::fromROSMsg(cloudInfo.cloud_deskewed, *cloudOut);
                pose6D p = trans2pose6D(transformTobeMapped);
                *cloudOut = *transformPointCloud2(cloudOut,  &p);
                publishCloud(&pubCloudRegisteredRaw, cloudOut, timeLaserInfoStamp, "odom");
            }
            // publish path
            if (pubPath.getNumSubscribers() != 0)
            {
                data->globalPath.header.stamp = timeLaserInfoStamp;
                data->globalPath.header.frame_id = "odom";
                pubPath.publish(data->globalPath);
            }

            // publish global map
            if (pubLaserCloudSurround.getNumSubscribers() != 0)
            {
                pcl::KdTreeFLANN<PointType>::Ptr kdtreeGlobalMap(new pcl::KdTreeFLANN<PointType>());;
                pcl::PointCloud<PointType>::Ptr globalMapKeyPoses(new pcl::PointCloud<PointType>());
                pcl::PointCloud<PointType>::Ptr globalMapKeyPosesDS(new pcl::PointCloud<PointType>());
                pcl::PointCloud<PointType>::Ptr globalMapKeyFrames(new pcl::PointCloud<PointType>());
                pcl::PointCloud<PointType>::Ptr globalMapKeyFramesDS(new pcl::PointCloud<PointType>());

                // kd-tree to find near key frames to visualize
                std::vector<int> pointSearchIndGlobalMap;
                std::vector<float> pointSearchSqDisGlobalMap;
                // search near key frames to visualize
                kdtreeGlobalMap->setInputCloud(data->cloudKeyPoses3D);
                kdtreeGlobalMap->radiusSearch(data->cloudKeyPoses3D->back(), 1000.0, pointSearchIndGlobalMap, pointSearchSqDisGlobalMap, 0); //globalMapVisualizationSearchRadius=1000,

                for (int i = 0; i < (int)pointSearchIndGlobalMap.size(); ++i)
                {
                    globalMapKeyPoses->push_back(data->cloudKeyPoses3D->points[pointSearchIndGlobalMap[i]]);
                }
                // downsample near selected key frames
                pcl::VoxelGrid<PointType> downSizeFilterGlobalMapKeyPoses; // for global map visualization
                downSizeFilterGlobalMapKeyPoses.setLeafSize(10.0, 10.0, 10.0); // globalMapVisualizationPoseDensity=10.0, for global map visualization
                downSizeFilterGlobalMapKeyPoses.setInputCloud(globalMapKeyPoses);
                downSizeFilterGlobalMapKeyPoses.filter(*globalMapKeyPosesDS);

                // extract visualized and downsampled key frames
                for (int i = 0; i < (int)globalMapKeyPosesDS->size(); ++i){
                    if (pointDistance(globalMapKeyPosesDS->points[i], data->cloudKeyPoses3D->back()) > 1000.0) //globalMapVisualizationSearchRadius=1000.0
                    if (pointDistance(globalMapKeyPosesDS->points[i], data->cloudKeyPoses3D->back()) > 1000.0) //globalMapVisualizationSearchRadius=1000.0
                        continue;
                    int thisKeyInd = (int)globalMapKeyPosesDS->points[i].intensity;
                    *globalMapKeyFrames += *transformPointCloud2(data->globalcorner_ptclvec[thisKeyInd],  &data->p6D[thisKeyInd]);
                    *globalMapKeyFrames += *transformPointCloud2(data->globalsurf_ptclvec[thisKeyInd],    &data->p6D[thisKeyInd]);
                }
                // downsample visualized points
                pcl::VoxelGrid<PointType> downSizeFilterGlobalMapKeyFrames; // for global map visualization
                downSizeFilterGlobalMapKeyFrames.setLeafSize(1.0, 1.0, 1.0); // globalMapVisualizationLeafSize=1.0, for global map visualization
                downSizeFilterGlobalMapKeyFrames.setInputCloud(globalMapKeyFrames);
                downSizeFilterGlobalMapKeyFrames.filter(*globalMapKeyFramesDS);
                publishCloud(&pubLaserCloudSurround, globalMapKeyFramesDS, timeLaserInfoStamp, "odom");
            }
        }


        pose6D trans2pose6D(float transformIn[])
        {
            pose6D p;
            p.x = transformIn[3];
            p.y = transformIn[4];
            p.z = transformIn[5];
            p.roll  = transformIn[0];
            p.pitch = transformIn[1];
            p.yaw   = transformIn[2];
            return p;
        }

        sensor_msgs::PointCloud2 publishCloud(ros::Publisher *thisPub, pcl::PointCloud<PointType>::Ptr thisCloud, ros::Time thisStamp, std::string thisFrame) 
        { 
            sensor_msgs::PointCloud2 tempCloud; 
            pcl::toROSMsg(*thisCloud, tempCloud); 
            tempCloud.header.stamp = thisStamp; 
            tempCloud.header.frame_id = thisFrame; 
            if (thisPub->getNumSubscribers() != 0) 
                thisPub->publish(tempCloud); 
            return tempCloud; 
        }

        //////////////////////////////////////// end #7 //////////////////////////////////////////


        /*
        bool saveMapService(lio_sam::save_mapRequest& req, lio_sam::save_mapResponse& res)
        {
          string saveMapDirectory;

          cout << "****************************************************" << endl;
          cout << "Saving map to pcd files ..." << endl;
          if(req.destination.empty()) saveMapDirectory = std::getenv("HOME") + savePCDDirectory;
          else saveMapDirectory = std::getenv("HOME") + req.destination;
          cout << "Save destination: " << saveMapDirectory << endl;
          // create directory and remove old files;
          int unused = system((std::string("exec rm -r ") + saveMapDirectory).c_str());
          unused = system((std::string("mkdir -p ") + saveMapDirectory).c_str());
          // save key frame transformations
          pcl::io::savePCDFileBinary(saveMapDirectory + "/trajectory.pcd", *cloudKeyPoses3D);
          pcl::io::savePCDFileBinary(saveMapDirectory + "/transformations.pcd", *cloudKeyPoses6D);
          // extract global point cloud map
          pcl::PointCloud<PointType>::Ptr globalCornerCloud(new pcl::PointCloud<PointType>());
          pcl::PointCloud<PointType>::Ptr globalCornerCloudDS(new pcl::PointCloud<PointType>());
          pcl::PointCloud<PointType>::Ptr globalSurfCloud(new pcl::PointCloud<PointType>());
          pcl::PointCloud<PointType>::Ptr globalSurfCloudDS(new pcl::PointCloud<PointType>());
          pcl::PointCloud<PointType>::Ptr globalMapCloud(new pcl::PointCloud<PointType>());
          for (int i = 0; i < (int)cloudKeyPoses3D->size(); i++) {
              *globalCornerCloud += *transformPointCloud(cornerCloudKeyFrames[i],  &cloudKeyPoses6D->points[i]);
              *globalSurfCloud   += *transformPointCloud(surfCloudKeyFrames[i],    &cloudKeyPoses6D->points[i]);
              cout << "\r" << std::flush << "Processing feature cloud " << i << " of " << cloudKeyPoses6D->size() << " ...";
          }

          if(req.resolution != 0)
          {
            cout << "\n\nSave resolution: " << req.resolution << endl;

            // down-sample and save corner cloud
            downSizeFilterCorner.setInputCloud(globalCornerCloud);
            downSizeFilterCorner.setLeafSize(req.resolution, req.resolution, req.resolution);
            downSizeFilterCorner.filter(*globalCornerCloudDS);
            pcl::io::savePCDFileBinary(saveMapDirectory + "/CornerMap.pcd", *globalCornerCloudDS);
            // down-sample and save surf cloud
            downSizeFilterSurf.setInputCloud(globalSurfCloud);
            downSizeFilterSurf.setLeafSize(req.resolution, req.resolution, req.resolution);
            downSizeFilterSurf.filter(*globalSurfCloudDS);
            pcl::io::savePCDFileBinary(saveMapDirectory + "/SurfMap.pcd", *globalSurfCloudDS);
          }
          else
          {
            // save corner cloud
            pcl::io::savePCDFileBinary(saveMapDirectory + "/CornerMap.pcd", *globalCornerCloud);
            // save surf cloud
            pcl::io::savePCDFileBinary(saveMapDirectory + "/SurfMap.pcd", *globalSurfCloud);
          }

          // save global point cloud map
          *globalMapCloud += *globalCornerCloud;
          *globalMapCloud += *globalSurfCloud;

          int ret = pcl::io::savePCDFileBinary(saveMapDirectory + "/GlobalMap.pcd", *globalMapCloud);
          res.success = ret == 0;

          downSizeFilterCorner.setLeafSize(mappingCornerLeafSize, mappingCornerLeafSize, mappingCornerLeafSize);
          downSizeFilterSurf.setLeafSize(mappingSurfLeafSize, mappingSurfLeafSize, mappingSurfLeafSize);

          cout << "****************************************************" << endl;
          cout << "Saving map to pcd files completed\n" << endl;

          return true;
        }
        */


    //////////////////////////////////////////////// LOOPCLOSURE FUNCTIONS ///////////////////////////////////////////////
    /*

        void loopClosureThread()
        {
            if (loopClosureEnableFlag == false)
                return;

            ros::Rate rate(loopClosureFrequency);
            while (ros::ok())
            {
                rate.sleep();
                performLoopClosure();
                visualizeLoopClosure();
            }
        }

        void loopInfoHandler(const std_msgs::Float64MultiArray::ConstPtr& loopMsg)
        {
            std::lock_guard<std::mutex> lock(mtxLoopInfo);
            if (loopMsg->data.size() != 2)
                return;

            loopInfoVec.push_back(*loopMsg);

            while (loopInfoVec.size() > 5)
                loopInfoVec.pop_front();
        }

        void performLoopClosure()
        {
            if (cloudKeyPoses3D->points.empty() == true)
                return;

            mtx.lock();
            *copy_cloudKeyPoses3D = *cloudKeyPoses3D;
            *copy_cloudKeyPoses6D = *cloudKeyPoses6D;
            mtx.unlock();

            // find keys
            int loopKeyCur;
            int loopKeyPre;
            if (detectLoopClosureExternal(&loopKeyCur, &loopKeyPre) == false)
                if (detectLoopClosureDistance(&loopKeyCur, &loopKeyPre) == false)
                    return;

            // extract cloud
            pcl::PointCloud<PointType>::Ptr cureKeyframeCloud(new pcl::PointCloud<PointType>());
            pcl::PointCloud<PointType>::Ptr prevKeyframeCloud(new pcl::PointCloud<PointType>());
            {
                loopFindNearKeyframes(cureKeyframeCloud, loopKeyCur, 0);
                loopFindNearKeyframes(prevKeyframeCloud, loopKeyPre, historyKeyframeSearchNum);
                if (cureKeyframeCloud->size() < 300 || prevKeyframeCloud->size() < 1000)
                    return;
                if (pubHistoryKeyFrames.getNumSubscribers() != 0)
                    publishCloud(&pubHistoryKeyFrames, prevKeyframeCloud, timeLaserInfoStamp, odometryFrame);
            }

            // ICP Settings
            static pcl::IterativeClosestPoint<PointType, PointType> icp;
            icp.setMaxCorrespondenceDistance(historyKeyframeSearchRadius*2);
            icp.setMaximumIterations(100);
            icp.setTransformationEpsilon(1e-6);
            icp.setEuclideanFitnessEpsilon(1e-6);
            icp.setRANSACIterations(0);

            // Align clouds
            icp.setInputSource(cureKeyframeCloud);
            icp.setInputTarget(prevKeyframeCloud);
            pcl::PointCloud<PointType>::Ptr unused_result(new pcl::PointCloud<PointType>());
            icp.align(*unused_result);

            if (icp.hasConverged() == false || icp.getFitnessScore() > historyKeyframeFitnessScore)
                return;

            // publish corrected cloud
            if (pubIcpKeyFrames.getNumSubscribers() != 0)
            {
                pcl::PointCloud<PointType>::Ptr closed_cloud(new pcl::PointCloud<PointType>());
                pcl::transformPointCloud(*cureKeyframeCloud, *closed_cloud, icp.getFinalTransformation());
                publishCloud(&pubIcpKeyFrames, closed_cloud, timeLaserInfoStamp, odometryFrame);
            }

            // Get pose transformation
            float x, y, z, roll, pitch, yaw;
            Eigen::Affine3f correctionLidarFrame;
            correctionLidarFrame = icp.getFinalTransformation();
            // transform from world origin to wrong pose
            Eigen::Affine3f tWrong = pclPointToAffine3f(copy_cloudKeyPoses6D->points[loopKeyCur]);
            // transform from world origin to corrected pose
            Eigen::Affine3f tCorrect = correctionLidarFrame * tWrong;// pre-multiplying -> successive rotation about a fixed frame
            pcl::getTranslationAndEulerAngles (tCorrect, x, y, z, roll, pitch, yaw);
            gtsam::Pose3 poseFrom = Pose3(Rot3::RzRyRx(roll, pitch, yaw), Point3(x, y, z));
            gtsam::Pose3 poseTo = pclPointTogtsamPose3(copy_cloudKeyPoses6D->points[loopKeyPre]);
            gtsam::Vector Vector6(6);
            float noiseScore = icp.getFitnessScore();
            Vector6 << noiseScore, noiseScore, noiseScore, noiseScore, noiseScore, noiseScore;
            noiseModel::Diagonal::shared_ptr constraintNoise = noiseModel::Diagonal::Variances(Vector6);

            // Add pose constraint
            mtx.lock();
            loopIndexQueue.push_back(make_pair(loopKeyCur, loopKeyPre));
            loopPoseQueue.push_back(poseFrom.between(poseTo));
            loopNoiseQueue.push_back(constraintNoise);
            mtx.unlock();

            // add loop constriant
            loopIndexContainer[loopKeyCur] = loopKeyPre;
        }

        bool detectLoopClosureDistance(int *latestID, int *closestID)
        {
            int loopKeyCur = copy_cloudKeyPoses3D->size() - 1;
            int loopKeyPre = -1;

            // check loop constraint added before
            auto it = loopIndexContainer.find(loopKeyCur);
            if (it != loopIndexContainer.end())
                return false;

            // find the closest history key frame
            std::vector<int> pointSearchIndLoop;
            std::vector<float> pointSearchSqDisLoop;
            kdtreeHistoryKeyPoses->setInputCloud(copy_cloudKeyPoses3D);
            kdtreeHistoryKeyPoses->radiusSearch(copy_cloudKeyPoses3D->back(), historyKeyframeSearchRadius, pointSearchIndLoop, pointSearchSqDisLoop, 0);
            
            for (int i = 0; i < (int)pointSearchIndLoop.size(); ++i)
            {
                int id = pointSearchIndLoop[i];
                if (abs(copy_cloudKeyPoses6D->points[id].time - timeLaserInfoCur) > historyKeyframeSearchTimeDiff)
                {
                    loopKeyPre = id;
                    break;
                }
            }

            if (loopKeyPre == -1 || loopKeyCur == loopKeyPre)
                return false;

            *latestID = loopKeyCur;
            *closestID = loopKeyPre;

            return true;
        }

        bool detectLoopClosureExternal(int *latestID, int *closestID)
        {
            // this function is not used yet, please ignore it
            int loopKeyCur = -1;
            int loopKeyPre = -1;

            std::lock_guard<std::mutex> lock(mtxLoopInfo);
            if (loopInfoVec.empty())
                return false;

            double loopTimeCur = loopInfoVec.front().data[0];
            double loopTimePre = loopInfoVec.front().data[1];
            loopInfoVec.pop_front();

            if (abs(loopTimeCur - loopTimePre) < historyKeyframeSearchTimeDiff)
                return false;

            int cloudSize = copy_cloudKeyPoses6D->size();
            if (cloudSize < 2)
                return false;

            // latest key
            loopKeyCur = cloudSize - 1;
            for (int i = cloudSize - 1; i >= 0; --i)
            {
                if (copy_cloudKeyPoses6D->points[i].time >= loopTimeCur)
                    loopKeyCur = round(copy_cloudKeyPoses6D->points[i].intensity);
                else
                    break;
            }

            // previous key
            loopKeyPre = 0;
            for (int i = 0; i < cloudSize; ++i)
            {
                if (copy_cloudKeyPoses6D->points[i].time <= loopTimePre)
                    loopKeyPre = round(copy_cloudKeyPoses6D->points[i].intensity);
                else
                    break;
            }

            if (loopKeyCur == loopKeyPre)
                return false;

            auto it = loopIndexContainer.find(loopKeyCur);
            if (it != loopIndexContainer.end())
                return false;

            *latestID = loopKeyCur;
            *closestID = loopKeyPre;

            return true;
        }

        void loopFindNearKeyframes(pcl::PointCloud<PointType>::Ptr& nearKeyframes, const int& key, const int& searchNum)
        {
            // extract near keyframes
            nearKeyframes->clear();
            int cloudSize = copy_cloudKeyPoses6D->size();
            for (int i = -searchNum; i <= searchNum; ++i)
            {
                int keyNear = key + i;
                if (keyNear < 0 || keyNear >= cloudSize )
                    continue;
                *nearKeyframes += *transformPointCloud(cornerCloudKeyFrames[keyNear], &copy_cloudKeyPoses6D->points[keyNear]);
                *nearKeyframes += *transformPointCloud(surfCloudKeyFrames[keyNear],   &copy_cloudKeyPoses6D->points[keyNear]);
            }

            if (nearKeyframes->empty())
                return;

            // downsample near keyframes
            pcl::PointCloud<PointType>::Ptr cloud_temp(new pcl::PointCloud<PointType>());
            downSizeFilterICP.setInputCloud(nearKeyframes);
            downSizeFilterICP.filter(*cloud_temp);
            *nearKeyframes = *cloud_temp;
        }

        void visualizeLoopClosure()
        {
            if (loopIndexContainer.empty())
                return;
            
            visualization_msgs::MarkerArray markerArray;
            // loop nodes
            visualization_msgs::Marker markerNode;
            markerNode.header.frame_id = odometryFrame;
            markerNode.header.stamp = timeLaserInfoStamp;
            markerNode.action = visualization_msgs::Marker::ADD;
            markerNode.type = visualization_msgs::Marker::SPHERE_LIST;
            markerNode.ns = "loop_nodes";
            markerNode.id = 0;
            markerNode.pose.orientation.w = 1;
            markerNode.scale.x = 0.3; markerNode.scale.y = 0.3; markerNode.scale.z = 0.3; 
            markerNode.color.r = 0; markerNode.color.g = 0.8; markerNode.color.b = 1;
            markerNode.color.a = 1;
            // loop edges
            visualization_msgs::Marker markerEdge;
            markerEdge.header.frame_id = odometryFrame;
            markerEdge.header.stamp = timeLaserInfoStamp;
            markerEdge.action = visualization_msgs::Marker::ADD;
            markerEdge.type = visualization_msgs::Marker::LINE_LIST;
            markerEdge.ns = "loop_edges";
            markerEdge.id = 1;
            markerEdge.pose.orientation.w = 1;
            markerEdge.scale.x = 0.1;
            markerEdge.color.r = 0.9; markerEdge.color.g = 0.9; markerEdge.color.b = 0;
            markerEdge.color.a = 1;

            for (auto it = loopIndexContainer.begin(); it != loopIndexContainer.end(); ++it)
            {
                int key_cur = it->first;
                int key_pre = it->second;
                geometry_msgs::Point p;
                p.x = copy_cloudKeyPoses6D->points[key_cur].x;
                p.y = copy_cloudKeyPoses6D->points[key_cur].y;
                p.z = copy_cloudKeyPoses6D->points[key_cur].z;
                markerNode.points.push_back(p);
                markerEdge.points.push_back(p);
                p.x = copy_cloudKeyPoses6D->points[key_pre].x;
                p.y = copy_cloudKeyPoses6D->points[key_pre].y;
                p.z = copy_cloudKeyPoses6D->points[key_pre].z;
                markerNode.points.push_back(p);
                markerEdge.points.push_back(p);
            }

            markerArray.markers.push_back(markerNode);
            markerArray.markers.push_back(markerEdge);
            pubLoopConstraintEdge.publish(markerArray);
        }

        void extractForLoopClosure()
        {
            pcl::PointCloud<PointType>::Ptr cloudToExtract(new pcl::PointCloud<PointType>());
            int numPoses = cloudKeyPoses3D->size();
            for (int i = numPoses-1; i >= 0; --i)
            {
                if ((int)cloudToExtract->size() <= surroundingKeyframeSize)
                    cloudToExtract->push_back(cloudKeyPoses3D->points[i]);
                else
                    break;
            }

            extractCloud(cloudToExtract);
        }

        void addLoopFactor()
        {
            if (loopIndexQueue.empty())
                return;

            for (int i = 0; i < (int)loopIndexQueue.size(); ++i)
            {
                int indexFrom = loopIndexQueue[i].first;
                int indexTo = loopIndexQueue[i].second;
                gtsam::Pose3 poseBetween = loopPoseQueue[i];
                gtsam::noiseModel::Diagonal::shared_ptr noiseBetween = loopNoiseQueue[i];
                gtSAMgraph.add(BetweenFactor<Pose3>(indexFrom, indexTo, poseBetween, noiseBetween));
            }

            loopIndexQueue.clear();
            loopPoseQueue.clear();
            loopNoiseQueue.clear();
            aLoopIsClosed = true;
        }
    */
    //////////////////////////////////////////////// END LOOPCLOSURE FUNCTIONS ///////////////////////////////////////////////
    };
    PLUGINLIB_EXPORT_CLASS(nodelet_ns::mapOptimization, nodelet::Nodelet);
}


/*
int main(int argc, char** argv)
{
    ros::init(argc, argv, "lio_sam");

    nodelet_ns::mapOptimization MO;

    ROS_INFO("\033[1;32m----> Map Optimization Started.\033[0m");
    
    //std::thread loopthread(&mapOptimization::loopClosureThread, &MO);
    //std::thread visualizeMapThread(&nodelet_ns::mapOptimization::visualizeGlobalMapThread, &MO);

    ros::spin();

    //loopthread.join();
    //visualizeMapThread.join();

    return 0;
}

*/
