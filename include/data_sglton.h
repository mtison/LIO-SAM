#include <iostream>
#include <stdint.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/Path.h>
#include <gtsam/geometry/Rot3.h>
#include <gtsam/geometry/Pose3.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/navigation/GPSFactor.h>
#include <gtsam/navigation/ImuFactor.h>
#include <gtsam/navigation/CombinedImuFactor.h>
#include <gtsam/nonlinear/NonlinearFactorGraph.h>
#include <gtsam/nonlinear/LevenbergMarquardtOptimizer.h>
#include <gtsam/nonlinear/Marginals.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>
#include <gtsam/nonlinear/ISAM2.h>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

typedef pcl::PointXYZI PointType;


typedef struct point6D
{
    double x;
    double y;
    double z;
    double roll;
    double pitch;
    double yaw;
    double idx;
    double time;
} pose6D;


class Data_sglton
{
public:
    //Members
    bool aLoopIsClosed = false;
    bool optimize_flag;
    boost::shared_mutex gps_mutex;
    boost::shared_mutex graph_mutex;
    std::deque<nav_msgs::Odometry> gpsQueue; // GPS data queue
    gtsam::NonlinearFactorGraph gtSAMgraph; // called 5x. Temporarly hold newest constraints up until they are added to the isam variable
    gtsam::Values initialEstimate; // called 4x. Temporarly hold newest node initial estimates up until they are added to the isam variable
    gtsam::ISAM2 *isam; // called 4x.  THE graph(pose path, full covariance, NOT THE KEYFRAMES) and the initial estimates of each node
    Eigen::MatrixXd poseCovariance; // called 1x. Covariance of the only the current pose

    pcl::PointCloud<PointType>::Ptr cloudKeyPoses3D;

    std::vector<pose6D> p6D;
    std::vector<pcl::PointCloud<PointType>::Ptr> globalcorner_ptclvec; // Vector of downsampled corner point clouds (one for each node) 
                                                                       // same number of clouds as the size of the graph in isam
    std::vector<pcl::PointCloud<PointType>::Ptr> globalsurf_ptclvec; // Vector of downsampled surface point clouds (one for each node)

    nav_msgs::Path globalPath;



    //Methods
    static Data_sglton *get();
    
private:
    //Members
    static Data_sglton *s_Instance;

    //Methods
    Data_sglton();
};
